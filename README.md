# Task

The task is about to understand what the code is doing, to document the most important things and to make recommendations how exactly the code can be optimized.

# Solution

## What Code Is Doing?

The code is attaching a transaction with invoice, hanlding multiple scenarios. One worth notice is, the invoices created before `8 July` and after `14 Sept` is mapped using the Invoice Number and later with customer name.

## Optimization

There are several things we can do in the different block of the provided code.

1. In the first `transactions` loop instead of pushing data to an object, we can use `array` and `bulkWrite` method of `mongoDB` to update the multiple documents a single query. This way we won't need `hasMultipleMatches` condition block.

2. Not sure what's there in `linkInvoiceWithTransactions` method but, instead of looping through the `invoices`, and link single document at a time, we can run `$in` condition to update multiple documents a single query and can pass all the invoice ids as array.

3. instead of putting the whole `filteredInvoices` array to the object we should only put the `id` in the `transactionsToInvoices` as we only need `id` and no other data later in the code.
