    static autolink() {
        console.log("Auto Link");
        
	let customerInvoices = CustomerInvoices.find({
            $where: `this.sum !== this.paid && this.status !== ${INVOICE_STATUSES.CANCELLATION}`
        }).fetch()
        
	let transactions = Transactions.find({ $where: 'this.sum !== this.paid' }).fetch()
        let transactionsToInvoices = {}
        
	transactions.forEach(transaction => {
            let date = moment(transaction.dateOfInvoice)
            //invoice mapping before 8 July and after 14 Sept is done on Invoice Number 
            let compareFunc = (date.isBefore(COMPARE_BY_INVOICE_NUMBER_BEFORE_DATE, 'day') || date.isAfter(COMPARE_BY_INVOICE_NUMBER_AFTER_DATE, 'day'))
                ? {
                    func: inv => {
                        if (!transaction.bookingDetails)
                            return false;

                        if (!inv.invoiceNumber)
                            return false;

                        return transaction.bookingDetails.includes(inv.invoiceNumber);
                    },
                    invoiceNumberCompared: true
                }
                : {
                    func: inv => {
                        if (transaction.sum !== inv.sum)
                            return false
                        let transCustomer = transaction.customerName.toLowerCase().trim()
                        let invCustomer = inv.customerName.toLowerCase().trim()

                        if (!transCustomer || !invCustomer)
                            return false;

                        if (
                            transCustomer === invCustomer ||
                            transCustomer.includes(invCustomer) ||
                            invCustomer.includes(transCustomer)
                        ) {
                            console.log(`linked ${invCustomer} with ${transCustomer}`)
                            return true
                        }

                        return false
                    }
                }

            let filteredInvoices = customerInvoices.filter(inv =>
                compareFunc.func(inv)
            )

            transactionsToInvoices[transaction._id] = compareFunc.invoiceNumberCompared
                ? {
                    filteredInvoices,
                    invoiceNumberCompared: compareFunc.invoiceNumberCompared
                } : {
                    filteredInvoices
                };

        })

        let hasMultipleMatches = []

        Object.keys(transactionsToInvoices).forEach(tranId => {
            if (transactionsToInvoices[tranId].filteredInvoices.length === 0) return
            if (transactionsToInvoices[tranId].filteredInvoices.length > 1) {
                if (transactionsToInvoices[tranId].invoiceNumberCompared) {
                    transactionsToInvoices[tranId].filteredInvoices.forEach(invoice => {
                        this.linkInvoiceWithTransactions({
                            invoiceId: invoice._id,
                            transactionsIds: [tranId]
                        })
                    });
                }
                else
                    hasMultipleMatches.push(tranId)

            } else {
                this.linkInvoiceWithTransactions({
                    invoiceId: transactionsToInvoices[tranId].filteredInvoices[0]._id,
                    transactionsIds: [tranId]
                })
            }
        })
        // console.log('hasMultipleMatches', hasMultipleMatches)
        if (hasMultipleMatches.length > 0) {
            Transactions.update(
                { _id: { $in: hasMultipleMatches } },
                { $set: { hasMultipleMatches: true } },
                { multi: true }
            )
        }
}

